module org.practicacuatro {
    requires javafx.controls;
    requires javafx.fxml;
    requires AnimateFX;
    opens org.practicacuatro to javafx.fxml;
    exports org.practicacuatro;
}