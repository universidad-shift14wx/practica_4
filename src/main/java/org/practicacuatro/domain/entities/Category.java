package org.practicacuatro.domain.entities;

import javafx.beans.property.SimpleStringProperty;
import org.practicacuatro.domain.entities.contracts.EntityInterface;

public class Category implements EntityInterface {

    public Category(String title) {
        this.title = new SimpleStringProperty(title);
    }

    private SimpleStringProperty title;

    public String getTitle() {
        return title.get();
    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    /**
     * @description
     * DATA CLASS EQUALITY
     */

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
