package org.practicacuatro.domain.entities;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import org.practicacuatro.domain.entities.contracts.EntityInterface;

public class Product implements EntityInterface {

    private SimpleStringProperty code;
    private SimpleStringProperty name;
    private SimpleStringProperty description;
    private SimpleStringProperty category;
    private SimpleDoubleProperty price;

    public Product(String code, String name, String description, String category, Double price) {
        this.code = new SimpleStringProperty(code);
        this.name = new SimpleStringProperty(name);
        this.description = new SimpleStringProperty(description);
        this.category = new SimpleStringProperty(category);
        this.price = new SimpleDoubleProperty(price);
    }

    public String getCode() {
        return code.get();
    }

    public SimpleStringProperty codeProperty() {
        return code;
    }

    public void setCode(String code) {
        this.code.set(code);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getCategory() {
        return category.get();
    }

    public SimpleStringProperty categoryProperty() {
        return category;
    }

    public void setCategory(String category) {
        this.category.set(category);
    }

    public double getPrice() {
        return price.get();
    }

    public SimpleStringProperty priceProperty() {
        return new SimpleStringProperty(String.valueOf(price.doubleValue()));
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    /**
     * @description
     * DATA CLASS EQUALITY
     */

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
