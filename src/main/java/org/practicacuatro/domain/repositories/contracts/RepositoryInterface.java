package org.practicacuatro.domain.repositories.contracts;

import javafx.collections.ObservableList;
import org.practicacuatro.domain.entities.contracts.EntityInterface;

public interface RepositoryInterface {

    ObservableList<?> getAll();

    void Add(EntityInterface entity);
    void Delete(EntityInterface entity);

}
