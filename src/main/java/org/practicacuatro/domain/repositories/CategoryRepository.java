package org.practicacuatro.domain.repositories;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.practicacuatro.domain.entities.Category;
import org.practicacuatro.domain.entities.contracts.EntityInterface;
import org.practicacuatro.domain.repositories.contracts.RepositoryInterface;

public class CategoryRepository implements RepositoryInterface {

    private ObservableList<Category> listCategory = FXCollections.observableArrayList();

    public CategoryRepository() {
        this.listCategory.add(new Category("Refrigerados"));
    }

    @Override
    public ObservableList<?> getAll() {
        return this.listCategory;
    }

    @Override
    public void Add(EntityInterface entity) {
        this.listCategory.add((Category) entity);
    }

    @Override
    public void Delete(EntityInterface entity) {

    }
}
