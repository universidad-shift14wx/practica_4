package org.practicacuatro.domain.repositories;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.practicacuatro.domain.entities.Product;
import org.practicacuatro.domain.entities.contracts.EntityInterface;
import org.practicacuatro.domain.repositories.contracts.RepositoryInterface;

public class ProductRepository implements RepositoryInterface {

    private Product producto;
    private ObservableList<Product> productsList = FXCollections.observableArrayList();

    public ProductRepository() {
        this.productsList.add(new Product("0000","Onion","its an onion what else you spec?","Refrigerados",0.10));
    }

    @Override
    public ObservableList<Product> getAll() {
        return productsList;
    }


    @Override
    public void Add(EntityInterface product) {
        this.producto = (Product) product;
        this.productsList.add(this.producto);
    }
    @Override
    public void Delete(EntityInterface product){
        this.productsList.remove(product);
    }
}
