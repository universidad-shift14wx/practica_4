package org.practicacuatro;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import animatefx.animation.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import org.practicacuatro.application.CategoryUseCase;
import org.practicacuatro.application.ProductoUseCase;
import org.practicacuatro.domain.entities.Category;
import org.practicacuatro.domain.entities.Product;
import org.practicacuatro.domain.repositories.CategoryRepository;
import org.practicacuatro.domain.repositories.ProductRepository;

public class PrimaryController implements Initializable {
    /**
     * TABLE DEFINITIONS
     */
    @FXML
    private TableView<Product> productTableView = new TableView<Product>();

    @FXML
    private TableColumn<Product,String> tcCode = new TableColumn<Product,String>();

    @FXML
    private TableColumn<Product,String> tcNombre = new TableColumn<Product,String>();

    @FXML
    private TableColumn<Product,String> tcCategoria = new TableColumn<Product,String>();

    @FXML
    private TableColumn<Product,String> tcPrecio = new TableColumn<Product,String>();

    @FXML
    private TableColumn<Product,String> tcDescripcion = new TableColumn<Product,String>();

    /**
     * FORM CONTROLS
     */
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtCode;
    @FXML
    private TextArea txtDescription;
    @FXML
    private ComboBox<String> cbCategroy;
    @FXML
    private Spinner<Double> spPrecio = new Spinner<Double>();
    @FXML
    private Button agregar;
    @FXML
    private Button btEliminar;

    enum estadoFormulario {
        AGREGAR,
        EDITAR
    }

    Product oldProduct;
    estadoFormulario estado = estadoFormulario.AGREGAR;
    /**
     * MISSELANIUS DEFINITIONS
     */

    private boolean ancla;
    @FXML
    AnchorPane addFormProductAnchorPane;
    @FXML
    AnchorPane errorAnchorPane;

    /**
     * USE CASE DEFINITIONS
     */
    final ProductoUseCase productoUseCase;
    final CategoryUseCase categoryUseCase;

    public PrimaryController() {
        this.productoUseCase = new ProductoUseCase(new ProductRepository());
        this.categoryUseCase = new CategoryUseCase(new CategoryRepository());
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.setupTable();
        this.setPrecioSpinner();
        this.setCategoryCombobox();
        /**
         * HIDE PANE FORM PRODUCT
         */
        new FadeOutDown(this.addFormProductAnchorPane).play();
        new FadeOutDown(this.errorAnchorPane).play();
        new FadeOutDown(this.btEliminar).play();
        this.ancla = false;
    }


    public void setPrecioSpinner(){
        this.spPrecio.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(Double.MIN_VALUE,Double.MAX_VALUE,0.01,0.01));
    }

    @FXML
    public void setCategoryCombobox(){
        this.cbCategroy.setItems(this.categoryUseCase.getAllCategoriesTitles());
    }

    public void setupTable(){
        this.tcNombre.setCellValueFactory(name->name.getValue().nameProperty());
        this.tcCode.setCellValueFactory(code->code.getValue().codeProperty());
        this.tcCategoria.setCellValueFactory(category->category.getValue().categoryProperty());
        this.tcPrecio.setCellValueFactory(precio->precio.getValue().priceProperty());
        this.tcDescripcion.setCellValueFactory(description->description.getValue().descriptionProperty());

        this.productTableView.setItems(this.productoUseCase.getAll());

        this.setTableListener();
    }



    public void setTableListener(){
        if(this.productTableView.getSelectionModel().getSelectedIndex()>-1){
            estado = estadoFormulario.EDITAR;
            this.oldProduct =this.productTableView.getSelectionModel().getSelectedItem();
            this.setTextFieldsFormProducto();
            this.showForm();
        }
    }

    /**
     * FORM ACTIONS
     */

    public void setTextFieldsFormProducto(){
        new FadeInUp(this.btEliminar).play();
        this.txtNombre.setText(this.oldProduct.getName().toString());
        this.txtDescription.setText(this.oldProduct.getDescription().toString());
        this.txtCode.setText(this.oldProduct.getCode().toString());
        this.spPrecio.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(Double.MIN_VALUE,Double.MAX_VALUE,this.oldProduct.getPrice(),0.01));
        this.cbCategroy.setValue(this.oldProduct.getCategory());
    }

    @FXML
    public void showForm(){
        new FadeInUp(this.addFormProductAnchorPane).play();
    }

    @FXML
    public void hideForm(){
        new FadeOutDown(this.addFormProductAnchorPane).play();
    }

    @FXML
    public void saveChangesForm(){

        this.executeForm();

    }

    public void executeForm(){
        if (!txtNombre.getText().equals("") && !this.txtCode.getText().equals("") && !this.cbCategroy.getValue().equals("") && !this.spPrecio.getValue().equals("") && !this.txtDescription.getText().equals("")){
            Product producto = new Product(
                    this.txtCode.getText(),
                    this.txtNombre.getText(),
                    this.txtDescription.getText(),
                    this.cbCategroy.getValue(),
                    this.spPrecio.getValue()
                );
            if(this.estado == estadoFormulario.AGREGAR){
                this.productoUseCase.Add(producto);
            }else{
                new FadeInUp(this.btEliminar).play();
                this.productoUseCase.Edit(producto,this.oldProduct);
                this.oldProduct = null;
            }
            this.ancla=false;
            this.cleanForm();
            new FadeOutDown(this.addFormProductAnchorPane).play();
        }else{
            if (this.ancla){
                new Shake(this.errorAnchorPane).play();
            }else{
                this.ancla=true;
                new FadeInLeft(this.errorAnchorPane).play();
            }
        }
    }

    public void cleanForm(){
        this.txtCode.setText("");
        this.txtNombre.setText("");
        this.txtDescription.setText("");
        this.setPrecioSpinner();
    }

    @FXML
    public void EliminarProducto(){
       try {
           this.productoUseCase.Delete(this.oldProduct);
       }catch (Exception e){
            throw e;
       }
        this.hideForm();
    }

}
