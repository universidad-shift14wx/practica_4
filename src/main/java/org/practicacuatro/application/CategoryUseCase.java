package org.practicacuatro.application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.practicacuatro.application.contracts.UseCase;
import org.practicacuatro.domain.entities.Category;
import org.practicacuatro.domain.entities.Product;
import org.practicacuatro.domain.entities.contracts.EntityInterface;
import org.practicacuatro.domain.repositories.contracts.RepositoryInterface;

public class CategoryUseCase implements UseCase {

    final RepositoryInterface categoryRepository;
    ObservableList<String> categoryTitleList = FXCollections.observableArrayList();
    /**
     * MAKE AN INDEPENDENCE INJECTION
     * @param categoryRepository
     */

    public CategoryUseCase(RepositoryInterface categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public boolean Add(EntityInterface entity) {
        return false;
    }

    public ObservableList<Category> getAll(){
        return (ObservableList<Category>) this.categoryRepository.getAll();
    }

    public ObservableList<String> getAllCategoriesTitles(){
        this.setTitles();
        return this.categoryTitleList;
    }

    public void setTitles(){
        this.getAll().forEach(categoria->{
            this.categoryTitleList.add(categoria.getTitle().toString());
        });
    }

}
