package org.practicacuatro.application;

import javafx.collections.ObservableList;
import org.practicacuatro.application.contracts.UseCase;
import org.practicacuatro.domain.entities.Product;
import org.practicacuatro.domain.entities.contracts.EntityInterface;
import org.practicacuatro.domain.repositories.contracts.RepositoryInterface;

public class ProductoUseCase implements UseCase {

    final RepositoryInterface productRepository;

    /**
     * MAKE AN INDEPENDENCE INJECTION
     * @param productRepository
     */

    public ProductoUseCase(RepositoryInterface productRepository) {
        this.productRepository = productRepository;
    }

    public ObservableList<Product> getAll(){
        return (ObservableList<Product>) this.productRepository.getAll();
    }

    public boolean Add(EntityInterface product){
        this.productRepository.Add(product);
        return true;
    }

    public boolean Edit(EntityInterface NewProduct,EntityInterface oldProduct){

        System.out.println(NewProduct);
        this.getAll().add((Product) NewProduct);
        this.getAll().remove((Product) oldProduct);
        return true;
    }

    public void Delete(EntityInterface productDelete){
            this.productRepository.Delete(productDelete);
    }


}
