package org.practicacuatro.application.contracts;

import javafx.collections.ObservableList;
import org.practicacuatro.domain.entities.contracts.EntityInterface;

public interface UseCase {

    public boolean Add(EntityInterface entity);

    public ObservableList<?> getAll();
}
